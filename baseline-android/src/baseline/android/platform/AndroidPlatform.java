package baseline.android.platform;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.BitmapFactory;
import baseline.platform.Log;
import baseline.platform.Platform;
import baseline.platform.SharedPreferences;

/**
 * Implementaci�n de la interface Platform para Android.
 * 
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class AndroidPlatform implements Platform {

	private final Log log;

	private final Context context;

	public AndroidPlatform(Context context, Log log) {
		this.context = context;
		this.log = log;
	}

	@Override
	public SharedPreferences open(String name) {
		SharedPreferences prefs = new AndroidSharedPreferences(name);

		return prefs;
	}

	@Override
	public InputStream openFileInput(String file) throws IOException {
		return context.openFileInput(file);
	}

	@Override
	public OutputStream openFileOutput(String file) throws IOException {
		return context.openFileOutput(file, Context.MODE_PRIVATE);
	}

	@Override
	public void deleteFile(String file) throws IOException {
		context.deleteFile(file);
	}

	class AndroidSharedPreferences implements SharedPreferences {
		private android.content.SharedPreferences prefs;
		private android.content.SharedPreferences.Editor editor;

		public AndroidSharedPreferences(String name) {
			prefs = context.getSharedPreferences(name, Context.MODE_PRIVATE);
		}

		@Override
		public void close() {
			if (editor != null) {
				editor.commit();
				editor = null;
			}
			prefs = null;
		}

		private void edit() {
			if (editor != null)
				return;
			editor = prefs.edit();
		}

		@Override
		public boolean getBoolean(String name, boolean defValue) {
			return prefs.getBoolean(name, defValue);
		}

		@Override
		public int getInt(String name, int defValue) {
			return prefs.getInt(name, defValue);
		}

		@Override
		public String getString(String name, String defValue) {
			return prefs.getString(name, defValue);
		}

		@Override
		public void putBoolean(String name, boolean value) {
			edit();
			editor.putBoolean(name, value);
		}

		@Override
		public void putInt(String name, int value) {
			edit();
			editor.putInt(name, value);
		}

		@Override
		public void putString(String name, String value) {
			edit();
			editor.putString(name, value);
		}
		
		@Override
		public void remove(String key) {
			edit();
			editor.remove(key);
		}

	}

	@Override
	public String getVersion() {
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
			return String.format("%s (%d)", info.versionName, info.versionCode);
		} catch (NameNotFoundException e) {
			log.warn("No se pudo obtener la version", e);
			return "Unknown";
		}
	}

	@Override
	public int getVersionCode() {
		try {
			return context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			log.warn(
					"No se pudo obtener el n�mero de version. Por defecto usamos 1",
					e);
			return 1;
		}
	}

	@Override
	public Object endodeBitmap(byte[] data) {
		if (data == null) {
			return null;
		}

		return BitmapFactory.decodeByteArray(data, 0, data.length);
	}
}
