package baseline.android.platform;

import baseline.android.BuildConfig;
import baseline.platform.Log;

/**
 * Implementacion del Log para android
 * 
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class AndroidLog extends Log {

	@Override
	protected void doDebug(String message, Throwable t) {
		android.util.Log.d("RSC", message, t);
	}

	@Override
	protected void doInfo(String message, Throwable t) {
		android.util.Log.i("RSC", message, t);
	}

	@Override
	protected void doWarn(String message, Throwable t) {
		android.util.Log.w("RSC", message, t);
	}

	@Override
	protected void doError(String message, Throwable t) {
		android.util.Log.e("RSC", message, t);
	}

	@Override
	public boolean isDebugEnabled() {
		return BuildConfig.DEBUG || android.util.Log.isLoggable("RSC", android.util.Log.VERBOSE) || android.util.Log.isLoggable("RSC", android.util.Log.DEBUG);
	}

	@Override
	public boolean isInfoEnabled() {
		return android.util.Log.isLoggable("RSC", android.util.Log.INFO);
	}

	@Override
	public boolean isWarnEnabled() {
		return android.util.Log.isLoggable("RSC", android.util.Log.WARN);
	}

	@Override
	public boolean isErrorEnabled() {
		return android.util.Log.isLoggable("RSC", android.util.Log.ERROR);
	}

}
