package baseline.android.storage;

import java.util.HashMap;
import java.util.Map;

import android.database.sqlite.SQLiteDatabase;
import baseline.platform.Log;
import baseline.platform.Platform;

/**
 * @author <a href="mailto:santiago.martinez@globant.com">Vidal Santiago
 *         Mart�nez</a>
 */
public class Compatibility {

	public static interface UpgradeStrategy {

		public void create(int oldVersion, SQLiteDatabase db,
				Platform platform, Log log) throws Exception;

		public void upgrade(int oldVersion, SQLiteDatabase db,
				Platform platform, Log log) throws Exception;

	}

	private static Map<Integer, UpgradeStrategy> strategies;

	// Map initializer.
	static {
		strategies = new HashMap<Integer, Compatibility.UpgradeStrategy>();

		// Development version (latest).
		strategies.put(0, new UpgradeStrategy11112013());
	}

	public static void create(int oldVersion, int newVersion,
			SQLiteDatabase db, Platform platform, Log log) throws Exception {

		UpgradeStrategy strategy = strategies.get(newVersion);

		if (strategy == null)
			// Default to lastest version known.
			strategy = new UpgradeStrategy11112013();

		strategy.create(oldVersion, db, platform, log);

	}

	public static void upgrade(int oldVersion, int newVersion,
			SQLiteDatabase db, Platform platform, Log log) throws Exception {
		UpgradeStrategy strategy = strategies.get(newVersion);

		if (strategy == null)
			// Default to lastest version known.
			strategy = new UpgradeStrategy11112013();

		strategy.upgrade(oldVersion, db, platform, log);
	}
}
