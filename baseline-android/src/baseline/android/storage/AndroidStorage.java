package baseline.android.storage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import baseline.platform.Log;
import baseline.platform.Platform;
import baseline.storage.Storage;
import baseline.storage.StorageException;

/**
 * @author <a href="mailto:santiago.martinez@globant.com">Vidal Santiago
 *         Mart�nez</a>
 */
public class AndroidStorage extends SQLiteOpenHelper implements Storage {

	private static final int CURRENT_VERSION = 0;
	private static final String DATABASE_NAME = "database_example.db";

	/* package */final class SQLiteTransaction extends Transaction {

		private boolean success;

		public SQLiteTransaction(boolean readOnly) {
			success = readOnly;
		}

		@Override
		public void success() {
			success = true;
		}

		public boolean isSuccess() {
			return success;
		}
	}

	private final int newVersion;
	private final Platform platform;
	private final Log log;

	public AndroidStorage(Context context, Platform platform, Log log) {
		super(context, DATABASE_NAME, null, platform.getVersionCode());
		this.newVersion = platform.getVersionCode();
		this.platform = platform;
		this.log = log;
	}

	@Override
	public Transaction startTransaction(boolean readonly) {

		log.debug(String.format("Started %s transaction", readonly ? "read"
				: "write"));

		SQLiteDatabase db = getWritableDatabase();

		db.beginTransaction();
		return new SQLiteTransaction(readonly);
	}

	@Override
	public Transaction startTransaction() {
		return startTransaction(true);
	}

	@Override
	public void endTransaction(Transaction t) {

		SQLiteTransaction tx = (SQLiteTransaction) t;

		log.debug(String.format("%s transaction", tx.isSuccess() ? "Committing"
				: "Rollingback"));

		SQLiteDatabase db = getWritableDatabase();

		if (tx.isSuccess())
			db.setTransactionSuccessful();

		db.endTransaction();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		log.info("Creating database...");

		try {
			Compatibility
					.create(CURRENT_VERSION, newVersion, db, platform, log);
			log.info("Database created");
		} catch (Exception e) {
			log.error("Unable to create database", e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		log.info(String.format(
				"Upgrading database from version %d to version %d...",
				oldVersion, newVersion));

		try {
			Compatibility.upgrade(oldVersion, newVersion, db, platform, log);
			log.info("Database successfully upgraded");
		} catch (Exception e) {
			log.error("Unable to upgrade database", e);
		}
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		log.info(String.format(
				"Downgrading database from version %d to version %d...",
				oldVersion, newVersion));

		try {
			Compatibility.upgrade(oldVersion, newVersion, db, platform, log);
			log.info("Database successfully downgraded");
		} catch (Exception e) {
			log.error("Unable to downgrade database", e);
		}

	}

	private void debugQueryPlan(String q) throws StorageException {
		// el "false" esta puesto a proposito porque en algunas versiones de
		// android esta generando problemas el "explain query plan".
		boolean debug = false;

		if (debug && log.isDebugEnabled()) {

			Cursor c = null;

			try {
				c = getWritableDatabase().rawQuery("explain query plan " + q,
						null);

				StringBuilder b = new StringBuilder("Query Plan:\n");

				while (c.moveToNext()) {
					int order = c.getInt(c.getColumnIndex("order"));
					int from = c.getInt(c.getColumnIndex("from"));
					String detail = c.getString(c.getColumnIndex("detail"));
					b.append(order).append('|').append(from).append('|')
							.append(detail).append("\n");
				}

				log.debug(b.toString());
			} catch (Exception e) {
				throw new StorageException("Unexpected db exception", e);
			} finally {
				if (c != null)
					c.close();
			}
		}
	}

	private static boolean intToBoolean(int i) {
		return i == 1 ? true : false;
	}

	private static int booleanToInt(boolean b) {
		return b ? 1 : 0;
	}

	/*
	 * Aca deber�an ir todos los metodos relacionados con la base de datos,
	 * obviamente aquellos metodos que esten especificados en la interface
	 * Storage
	 */
}
