package baseline.android.storage;

import android.database.sqlite.SQLiteDatabase;
import baseline.android.storage.Compatibility.UpgradeStrategy;
import baseline.platform.Log;
import baseline.platform.Platform;

/**
 * @author <a href="mailto:santiago.martinez@globant.com">Vidal Santiago
 *         Mart�nez</a>
 */
public class EmptyStrategy implements UpgradeStrategy {

	@Override
	public void create(int oldVersion, SQLiteDatabase db, Platform platform,
			Log log) throws Exception {

	}

	@Override
	public void upgrade(int oldVersion, SQLiteDatabase db, Platform platform,
			Log log) throws Exception {

	}

}
