package baseline.android.storage;

import android.database.sqlite.SQLiteDatabase;
import baseline.android.storage.Compatibility.UpgradeStrategy;
import baseline.platform.Log;
import baseline.platform.Platform;

/**
 * @author <a href="mailto:santiago.martinez@globant.com">Vidal Santiago
 *         Mart�nez</a>
 */
public class UpgradeStrategy11112013 implements UpgradeStrategy {

	/**
	 * Tabla settings, guarda toda la informaci�n relaciona la configuraci�n y
	 * perfil de usuario, la estuctura de la tabla es similar a la de un hash
	 * <clave, valor>.
	 * 
	 * Motivo por el cual no requiere cambio de estuctura.
	 */
	/* package */static final String TABLE_SETTING = "setting";

	/**
	 * Tabla contact, almacena toda la informaci�n referia a un contacto,
	 * tambi�n guarda la foto del contacto.
	 */
	/* package */static final String TABLE_CONTACT = "contact";

	/**
	 * Tabla group, almacena toda la informaci�n referida a los grupos, esta
	 * informaci�n luego sera utilizada para otener los post de cada uno de los
	 * grupos. Es importante notar que JAM no distingue entre grupos y canales
	 * por ello, ambos tipos se guardarn en la misma tabla, y seran ditiguibles
	 * unos de otros por el campo "acl".
	 */
	/* package */static final String TABLE_GROUP = "`group`";

	/**
	 * Tabla post almacena todos los post que se hicieron en cada uno de los
	 * grupos, recordar que no distinguimos como entidades distintas a los
	 * grupos y a los post, ya que son exactamente lo mismo con diferente nivel
	 * de seguridad, por ello los comentarios se procede de la misma forma.
	 * 
	 * La estrutura de los post y comentarios son iguales, tanto para grupos
	 * como para canales.
	 */
	/* package */static final String TABLE_POST = "post";

	/**
	 * Tabla utilizada para guardar los mensajes enviados a un contacto, esto es
	 * comunicacion 1 a 1 (o chat).
	 */
	/* package */static final String TABLE_MESSAGE = "message";

	@Override
	public void create(int oldVersion, SQLiteDatabase db, Platform platform,
			Log log) throws Exception {

		/*
		 * Definici�n de las tablas
		 */
		log.debug("Creando la tabla settings");
		db.execSQL("CREATE TABLE setting (name TEXT PRIMARY KEY NOT NULL, value TEXT)");

		log.debug("Creando la tabla contact");
		db.execSQL("CREATE TABLE contact (contact_id INTEGER PRIMARY KEY, first_name TEXT NOT NULL, last_name TEXT, email TEXT, phone TEXT, image_preview BLOB, active INTEGER DEFAULT 1)");

		log.debug("Creando la tabla group");
		db.execSQL("CREATE TABLE `group` (group_id INTEGER PRIMARY KEY NOT NULL, name TEXT NOT NULL, description TEXT, last_activity INTEGER,"
				+ " created_at INTEGER, creator_id INTEGER, read_only INTEGER, image_preview BLOB, active INTEGER DEFAULT 1)");

		log.debug("Creando la tabla post");
		db.execSQL("CREATE TABLE post (post_id INTEGER PRIMARY KEY NOT NULL, parent_post_id INTEGER, group_id INTEGER NOT NULL, title TEXT NOT NULL, content TEXT,"
				+ " published INTEGER, updated INTEGER, author_id INTEGER, read INTEGER, preview BLOB, enclosure BLOB, enclosure_type TEXT, active INTEGER DEFAULT 1, status TEXT)");

		/*
		 * Definici�n de indicies de las tablas, estos indices mejoran
		 * significativamente la performance de la tabla, por ello es importante
		 * revisar el "Explain Query" en el Log y corroborar que se esten
		 * utilizando estos indices, si alguna de las consultas no lo hace, o
		 * bien, conviene reformular la consulta para que seleccione campos que
		 * formar el indice, o bien crear un nuevo indice con el campo que se
		 * esta usando en la consulta sin optimizar.
		 */
		log.debug("Creando indice para la tabla contact");
		db.execSQL("CREATE INDEX contact_id_idx ON contact(contact_id)");

		log.debug("Creando indice para la tabla group");
		db.execSQL("CREATE INDEX group_id_idx ON `group`(group_id)");

		log.debug("Creando indice para la tabla post");
		db.execSQL("CREATE INDEX post_id_idx ON post(post_id)");
	}

	@Override
	public void upgrade(int oldVersion, SQLiteDatabase db, Platform platform,
			Log log) throws Exception {
		create(oldVersion, db, platform, log);
	}
}
