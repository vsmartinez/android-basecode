package baseline.android.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.webkit.MimeTypeMap;

/**
 * @author <a href="mailto:santiago.martinez@globant.com">Vidal Santiago
 *         Mart�nez</a>
 */
public class PhotoStorageUtils {

	private static Uri getUri(String id, String mimeType) {

		if (!Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())
				|| Environment.MEDIA_MOUNTED_READ_ONLY.equals(Environment
						.getExternalStorageState()))
			return null;

		return Uri
				.parse("file://"
						+ Environment.getExternalStorageDirectory()
								.getAbsolutePath()
						+ "/Pictures/Album/"
						+ id
						+ "."
						+ MimeTypeMap.getSingleton().getExtensionFromMimeType(
								mimeType));
	}

	public static String getMimeForFile(Uri uri) {
		try {
			return MimeTypeMap.getFileExtensionFromUrl(new File(new URI(uri
					.toString())).getAbsolutePath());
		} catch (URISyntaxException e) {
			// Unknown mime type.
			return "*/*";
		}
	}

	/**
	 * Returns the String path where the image was saved.
	 */
	public static String save(Context context, String id,
			byte[] imageAttachment, String mime) throws IOException {
		Uri uri = getUri(id, mime);

		if (uri == null)
			throw new IOException("External storage not ready");

		File file = null;

		try {
			file = new File(new URI(uri.toString()));
		} catch (URISyntaxException e) {
			throw new IOException(e.getMessage());
		}

		FileOutputStream fos = null;

		try {
			file.getParentFile().mkdirs();
			file.createNewFile();

			fos = new FileOutputStream(file);

			fos.write(imageAttachment);
			fos.flush();

		} finally {
			if (fos != null)
				try {
					fos.close();
				} catch (Exception e) {
					// Ignored
				}
		}

		refreshMedia(context, file, mime);

		return file.getAbsolutePath();
	}

	private static void refreshMedia(Context context, File file, String mime) {

		new MediaScannerWrapper(context, file.getAbsolutePath(), mime).scan();
	}

	private static class MediaScannerWrapper implements
			MediaScannerConnection.MediaScannerConnectionClient {
		private MediaScannerConnection connection;
		private String path;
		private String mime;

		public MediaScannerWrapper(Context context, String path, String mime) {
			this.path = path;
			this.mime = mime;
			connection = new MediaScannerConnection(context, this);
		}

		public void scan() {
			connection.connect();
		}

		public void onMediaScannerConnected() {
			connection.scanFile(path, mime);
		}

		public void onScanCompleted(String path, Uri uri) {
			connection.disconnect();
		}
	}
}
