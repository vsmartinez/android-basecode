package baseline.android.utils;

/**
 * @author <a href="mailto:santiago.martinez@globant.com">Vidal Santiago
 *         Mart�nez</a>
 */
public class Constants {

	public static String shortDateTimeFormat = "HH:mm - dd/MM/yyyy";

	public static String dateFormat = "dd/MM/yyyy";

	public static String simpleDatePostFormat = "HH:mm aa";

}
