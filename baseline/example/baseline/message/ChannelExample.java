package baseline.message;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import baseline.message.builder.ChannelBuilder;
import baseline.message.event.Event;

public class ChannelExample {

	public static void main(String[] args) throws InterruptedException {

		// ChannelManager instance
		final ChannelManager cm = new ChannelManager();

		// Start the engine
		cm.start();

		// Creating a channel which match with any event's topic.
		final Channel c1 = new ChannelBuilder().topic("All events goes here")
				.pattern(Pattern.compile(".*")).build();

		// Creating a channel which match with those topics which start with
		// one or many "a" followed by one "b"
		final Channel c2 = new ChannelBuilder()
				.topic("Many 'a' followed by one 'b'")
				.pattern(Pattern.compile("a*b")).build();

		// Adding channels to ChannelManager
		try {
			cm.addChannel(c1);
			cm.addChannel(c2);
		} catch (AlreadyExistChannel e) {

		}

		// Retrieve and shows the list of available channels
		System.out.println("List of available channels");
		List<Channel> lc = cm.getChannels();
		for (Channel c : lc) {
			System.out.println(String.format("Pattern: %s Topic: %s",
					c.pattern(), c.getTopic()));
		}

		// Publishing a bunch of events randomly generated
		System.out.println("Event progress:");

		// Subscribing to channel 1
		cm.subscribe(c1, new Subscriber() {

			@Override
			public void notify(Object payload) {
				System.out.println(c1.getTopic() + ": " + payload);
			}
		});

		// Subscribing to channel 2
		cm.subscribe(c2, new Subscriber() {

			@Override
			public void notify(Object payload) {
				System.out.println(c2.getTopic() + ": " + payload);
			}
		});

		Random r = new Random(System.currentTimeMillis());

		// List of event example
		List<Event> events = new ArrayList<Event>();
		for (int i = 0; i < 100; i++) {
			if (r.nextBoolean()) {
				events.add(new Event("shouldn't match with channel", "invalid event with LOW priority",
						Event.LOW));
			} else {
				events.add(new Event("ab", "valid event with HIGH priority", Event.HIGH));
			}
		}

		// Publishing the event list
		cm.publish(events);

		// Publishing event one by one
		for (int i = 0; i < 100; i++) {
			if (r.nextBoolean()) {
				cm.publish(new Event("shouldn't match with channel neither", "invalid event with LOW priority too",
						Event.LOW));
			} else {
				cm.publish(new Event("aab", "valid event with HIGH priority too", Event.HIGH));
			}
		}

		// Waiting for print out
		Thread.sleep(1000);

		// Stopping the ChannelManager instance,
		cm.stop();

		// Event published after stop, will not be published
		cm.publish(new Event("These events shouldn't be dispached", "useless event with HIGH priority",
				Event.HIGH));

	}
}
