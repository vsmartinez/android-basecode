package baseline.request;

import json.parser.JsonParser;
import baseline.request.part.MimeType;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class GetExampleRequest extends BaseRequest<Boolean> {

	public GetExampleRequest(String param) {
	}
	
	@Override
	public Boolean processContent(Response response) throws Exception {
		return JsonParser.fromJson(Boolean.class, response.asString());
	}

	@Override
	public String getPath() {
		return "/api/get";
	}

	@Override
	public HttpMethod getMethod() {
		return HttpMethod.GET;
	}

	@Override
	public String getContentType() {
		return MimeType.json.contentType;
	}

	@Override
	public String getAcceptType() {
		return MimeType.json.contentType;
	}
}
