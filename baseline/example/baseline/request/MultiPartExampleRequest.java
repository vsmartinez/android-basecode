package baseline.request;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import json.parser.JsonParser;
import baseline.request.BaseRequest;
import baseline.request.Response;
import baseline.request.part.MimeType;
import baseline.request.part.MultiPart.ContentDisposition;
import baseline.request.part.builder.InputStreamPartBuilder;
import baseline.request.part.builder.MultiPartBuilder;
import baseline.request.part.builder.SimplePartBuilder;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class MultiPartExampleRequest extends BaseRequest<String> {

	@Override
	public String processContent(Response response) throws Exception {
		return JsonParser.fromJson(String.class, response.asString());
	}

	@Override
	public String getPath() {
		return "/api/upload";
	}

	@Override
	public HttpMethod getMethod() {
		return HttpMethod.POST;
	}

	@Override
	public String getContentType() {
		return MimeType.json.contentType;
	}

	@Override
	public String getAcceptType() {
		return MimeType.json.contentType;
	}

	@Override
	public void writeBody(OutputStream out) throws Exception {
		new MultiPartBuilder("boundary", ContentDisposition.FORM_DATA)
				.addPart(
						new SimplePartBuilder("Santiago").addParameter("name",
								"first-name").build())
				.addPart(
						new InputStreamPartBuilder(
								new FileInputStream(
										new File(
												"example/baseline/request/test.txt")),
								MimeType.text).build()).build().write(out);
	}
}
