package baseline.request.proxymock;

import baseline.cache.CacheRequest;
import baseline.cache.InMemoryCacheStorage;
import baseline.request.BaseRequest;
import baseline.request.GetExampleRequest;
import baseline.request.MultiPartExampleRequest;
import baseline.request.Request;
import baseline.request.RequestException;
import baseline.request.RequestExecutor;
import baseline.request.RequestManager;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class ProxyMockExample {

	public static void main(String[] args) throws RequestException {

		ProxyMock pm = new ProxyMock();
		Request<Boolean> mock = pm.mock(GetExampleRequest.class);

		pm.when(mock.getMethod()).thenReturn(Request.HttpMethod.POST);
		pm.when(mock.execute()).thenReturn(true);
		pm.when(mock.getContentType()).thenReturn("plain/html");

		System.out.println(mock.getMethod());
		System.out.println(mock.getAcceptType());
		System.out.println(mock.getContentType());

		BaseRequest.setRequestManager(new RequestManager("http://localhost"));

		RequestExecutor re = new RequestExecutor(new CacheRequest(
				new InMemoryCacheStorage()));

		Boolean r = re.sync(mock);
		System.out.println("Fake response: " + r);

		ProxyMock pm2 = new ProxyMock();
		Request<String> mockException = pm2.mock(MultiPartExampleRequest.class);

		pm2.when(mockException.execute()).thenThrowException(
				new RequestException("This exception is not real."));
		re.sync(mockException);
	}
}
