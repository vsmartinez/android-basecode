package baseline.pipeline;

import java.util.List;
import java.util.Random;

import baseline.cache.CacheRequest;
import baseline.cache.InMemoryCacheStorage;
import baseline.message.Channel;
import baseline.message.ChannelManager;
import baseline.request.BaseRequest;
import baseline.request.GetExampleRequest;
import baseline.request.PipelinedRequestExecutor;
import baseline.request.Request;
import baseline.request.RequestException;
import baseline.request.RequestExecutor;
import baseline.request.RequestExecutor.ExecutorListener;
import baseline.request.RequestManager;
import baseline.request.proxymock.ProxyMock;

public class PipelinedRequestExecutorExample {

	public static void main(String[] args) throws RequestException,
			InterruptedException {
		ProxyMock pm = new ProxyMock();
		Request<Boolean> mock = pm.mock(GetExampleRequest.class);

		pm.when(mock.execute()).thenReturn(true);

		final ChannelManager cm = new ChannelManager();

		BaseRequest.setRequestManager(new RequestManager("http://localhost"));

		final RequestExecutor re = new RequestExecutor(new CacheRequest(
				new InMemoryCacheStorage()));

		PipelinedRequestExecutor plre = new PipelinedRequestExecutor(re, cm);

		// Start engine
		plre.start();

		Random r = new Random(System.currentTimeMillis());

		for (int i = 0; i < 100; i++) {
			final int p = r.nextInt(10);
			final int j = i;
			plre.publish(mock, p,
					new ExecutorListener<Boolean, RequestException>() {

						@Override
						public void succeed(Boolean t) {
							System.out.println(String
									.format("Order: %02d - Priority: %02d - Fake response: %s",
											j, p, t));
						}

						@Override
						public void failure(RequestException e) {
							System.err.println("Error: " + e.getMessage());
						}
					});
		}

		Thread.sleep(1000);

		// To check if there is a remaining channel
		List<Channel> cs = cm.getChannels();
		for (Channel c : cs) {
			System.out.println(c.getTopic() + " " + c.pattern());
		}

		plre.stop();
	}
}
