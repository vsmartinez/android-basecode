package baseline.pipeline;

import java.util.ArrayList;
import java.util.List;

import baseline.cache.CacheRequest;
import baseline.cache.InMemoryCacheStorage;
import baseline.message.Channel;
import baseline.message.ChannelManager;
import baseline.request.BaseRequest;
import baseline.request.GetExampleRequest;
import baseline.request.PipelinedRequestExecutor;
import baseline.request.Request;
import baseline.request.RequestException;
import baseline.request.RequestExecutor;
import baseline.request.RequestExecutor.ExecutorListener;
import baseline.request.RequestManager;
import baseline.request.proxymock.ProxyMock;

public class PipelinedRequestExecutorExampleList {

	public static void main(String[] args) throws RequestException,
			InterruptedException {
		ProxyMock pm = new ProxyMock();
		Request<Boolean> mock = pm.mock(GetExampleRequest.class);

		pm.when(mock.execute()).thenReturn(true);

		final ChannelManager cm = new ChannelManager();

		BaseRequest.setRequestManager(new RequestManager("http://localhost"));

		final RequestExecutor re = new RequestExecutor(new CacheRequest(
				new InMemoryCacheStorage()));

		PipelinedRequestExecutor plre = new PipelinedRequestExecutor(re, cm);

		// Start engine
		plre.start();

		List<Request<Boolean>> lr = new ArrayList<Request<Boolean>>();

		for (int i = 0; i < 100; i++) {
			lr.add(mock);
		}

		plre.publish(lr, new ExecutorListener<Boolean, RequestException>() {

			@Override
			public void succeed(Boolean t) {
				System.out.println(String.format("Fake response: %s", t));
			}

			@Override
			public void failure(RequestException e) {
				System.err.println("Error: " + e.getMessage());
			}
		});

		Thread.sleep(1000);

		// To check if there is a remaining channel
		List<Channel> cs = cm.getChannels();
		for (Channel c : cs) {
			System.out.println(c.getTopic() + " " + c.pattern());
		}

		plre.stop();
	}
}
