package baseline.platform;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public abstract class Log {

	// DEBUG

	public boolean isDebugEnabled() {
		return true;
	}

	protected abstract void doDebug(String message, Throwable t);

	public final void debug(String message) {
		debug(message, null);
	}

	public final void debug(String message, Throwable t) {
		if (isDebugEnabled())
			doDebug(message, t);
	}

	// INFO

	public boolean isInfoEnabled() {
		return true;
	}

	protected abstract void doInfo(String message, Throwable t);

	public final void info(String message) {
		info(message, null);
	}

	public final void info(String message, Throwable t) {
		if (isInfoEnabled())
			doInfo(message, t);
	}

	// WARN

	public boolean isWarnEnabled() {
		return true;
	}

	protected abstract void doWarn(String message, Throwable t);

	public final void warn(String message) {
		warn(message, null);
	}

	public final void warn(String message, Throwable t) {
		if (isWarnEnabled())
			doWarn(message, t);
	}

	// ERROR

	public boolean isErrorEnabled() {
		return true;
	}

	protected abstract void doError(String message, Throwable t);

	public final void error(String message) {
		warn(message, null);
	}

	public final void error(String message, Throwable t) {
		if (isErrorEnabled())
			doError(message, t);
	}
}