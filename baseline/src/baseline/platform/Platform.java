package baseline.platform;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Platform methods
 *
 * Methods whom may vary on each platform
 * 
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public interface Platform {

	String getVersion();

	int getVersionCode();

	/**
	 * @throws IOException
	 *             if unable to open the file.
	 */
	SharedPreferences open(String name) throws IOException;

	/**
	 * @throws IOException
	 *             if unable to create the file.
	 */
	OutputStream openFileOutput(String file) throws IOException;

	/**
	 * @throws IOException
	 *             if the file doesn't exist.
	 */
	InputStream openFileInput(String file) throws IOException;

	/**
	 * @throws IOException
	 *             if the file doesn't exist.
	 */
	void deleteFile(String file) throws IOException;

	/**
	 * Transform a byte array to a bitmap, based on the underlying platform
	 * 
	 * @param data
	 * @return
	 */
	Object endodeBitmap(byte[] data);

}
