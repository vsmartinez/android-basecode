package baseline.platform;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public interface SharedPreferences {
	int getInt(String name, int defValue);

	String getString(String string, String defValue);

	boolean getBoolean(String bool, boolean defValue);

	void putInt(String name, int value);

	void putString(String name, String value);

	void putBoolean(String string, boolean value);
	
	void remove(String key);

	void close();
}
