package baseline.platform;

public class ConsoleLog extends Log {

	@Override
	protected void doWarn(String message, Throwable t) {
		System.out.println("[warn] " + message);
	}

	@Override
	protected void doInfo(String message, Throwable t) {
		System.out.println("[info] " + message);
	}

	@Override
	protected void doError(String message, Throwable t) {
		System.out.println("[error] " + message);
	}

	@Override
	protected void doDebug(String message, Throwable t) {
		System.out.println("[debug] " + message);
	}
}
