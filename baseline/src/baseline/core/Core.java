package baseline.core;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import json.parser.JsonParser;

import baseline.cache.CacheRequest;
import baseline.cache.CacheRequest.ResponseFilter;
import baseline.cache.InMemoryCacheStorage;
import baseline.platform.Log;
import baseline.platform.Platform;
import baseline.request.BaseRequest;
import baseline.request.JsonBaseRequest;
import baseline.request.MultiPartExampleRequest;
import baseline.request.Request;
import baseline.request.RequestException;
import baseline.request.RequestExecutor;
import baseline.request.RequestManager;
import baseline.request.Response;

public class Core {

    private Platform platform;
    private Log log;
    private RequestExecutor requestExecutor;

    public static class ListenerManager extends CoreListener {
        /**
         * <p>
         * This class hold listeners to be triggered when core did some change and must notify
         * to the subscribers.
         * <p/>
         * <p>
         * Due subscribe/un-subscribe as well as event propagation are asynchronous,
         * the list which hold the listener MUST iterated into a synchronized block.
         * <p/>
         * <p>Any access to this list must be in a synchronized way, if you want to iterate over
         * listener list, use the following example, to avoid deadlock or unpredictable code run
         * <p/>
         * <pre>
         * synchronized (listeners) {
         * 	Iterator i = list.iterator(); // iterator must be placed into a synchronized block
         * 	while (i.hasNext())
         * 		foo(i.next());
         * }
         * </pre>
         * <p>If you do not follow this recommendation, I can't guarantee your code is going to work
         * as expected</p>
         */
        private List<CoreListener> listeners = Collections
                .synchronizedList(new ArrayList<CoreListener>());

        public void addListener(CoreListener listener) {
            if (listeners.contains(listener))
                return;
            listeners.add(listener);
        }

        public void removeListener(CoreListener listener) {
            if (!listeners.contains(listener))
                return;
            listeners.remove(listener);
        }

        /**
         * Override CoreListener
         */
        /**
         * Write here all those method you are going to use to notify the other layers
         *
         * <pre>
         * public void foo() {
         * 	synchronized (listeners) {
         * 		for (CoreListener listener : this.listeners) {
         * 			listener.foo();
         *        }
         *    }
         * }
         * </pre>
         */
    }

    public Core(Platform platform, Log log) {
        this.platform = platform;
        this.log = log;

        this.log.info(String.format("Iniciando Core %s %s",
                this.platform.getVersion(), this.platform.getVersionCode()));

        BaseRequest.setRequestManager(new RequestManager("http://localhost"));

        CacheRequest cr = new CacheRequest(new InMemoryCacheStorage(), log);
        cr.setFilter(new ResponseFilter() {

            @Override
            public boolean keep(Request<?> request, Object element) {
                return false;
            }

        });

        requestExecutor = new RequestExecutor(cr, log);
    }


    public void setNetworkLimit(final int networkLimit) {
        requestExecutor.setNetworkLimit(networkLimit);
    }

    /**
     * Example about how to use requestExecutor instance
     */
    public void foo() {
        // Do what ever you want here, also you can do some request.
        requestExecutor.async(new JsonBaseRequest<Boolean>() {

            @Override
            public Boolean processContent(Response response)
                    throws Exception {
                return true;
            }

            @Override
            public String getPath() {
                return "/example/add";
            }

            @Override
            public HttpMethod getMethod() {
                return HttpMethod.POST;
            }

            @Override
            public void writeBody(OutputStream out) throws Exception {

                /**
                 * JsonParser will generate:
                 *
                 * <pre>
                 * {
                 * 	 "user" : { "first-name" : "Santiago",
                 * 				"last-name"  : "Martinez" }
                 * }
                 * </pre>
                 */
                JsonParser jp = new JsonParser().object("user")
                        .add("first-name", "Santiago")
                        .add("last-name", "Martinez").end();

                out.write(jp.toJson().getBytes());
            }
        });
    }

    public void foo2() {
        try {
            requestExecutor.sync(new MultiPartExampleRequest());
        } catch (RequestException e) {
            e.printStackTrace();
        }
    }
}
