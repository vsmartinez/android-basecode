package baseline.core;

/**
 * Clase concreta que actua como listener/callback.
 * 
 * Provee una implementacion por defecto para todos los metodos donde ninguno de
 * ellos hacen algo, de esta forma evitamos que las clases que hereden de esta
 * clase tengan que obligatoramiente implementar todos los metodos, en cambio,
 * solo deberan sobreescribir aquellos metodos en los cuales se esta interesado
 * realizar alguna accion especifica.
 * 
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>  
 */
public class CoreListener {

	/**
	 * Example:
	 * 
	 * <pre>
	 * public void foo() {
	 * 	// Nothing to do, override at subclass if needed
	 * }
	 * </pre>
	 */

}
