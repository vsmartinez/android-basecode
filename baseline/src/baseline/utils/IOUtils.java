package baseline.utils;

import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public final class IOUtils {

	private IOUtils() {
	}

	public static void close(Closeable... closeables) {
		for (Closeable closeable : closeables) {
			if (closeable != null)
				try {
					closeable.close();
				} catch (IOException e) {
				}
		}
	}

	public static void copy(Reader reader, Writer writer) throws IOException {

		char[] buff = new char[4096];
		while (reader.read(buff) > 0)
			writer.write(buff);

	}

}
