package baseline.message.filter;

import baseline.message.event.Event;

/**
 * <p>
 * Filter to register into a channel
 * <p>
 * This interface aim to give you a better handling of which event must be
 * published or discarded.
 * 
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 * 
 */
public interface Filter {
	/**
	 * Check if event has to be discarded
	 * 
	 * @param topic
	 *            to match against a channel
	 * @param event
	 *            to send if topic match with at least one channel
	 * @return true if Event has to be discarded, true otherwise
	 */
	public boolean discard(String topic, Event event);
}
