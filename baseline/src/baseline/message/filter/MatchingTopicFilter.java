package baseline.message.filter;

import baseline.message.event.Event;

public class MatchingTopicFilter implements Filter {

	@Override
	public boolean discard(String topic, Event event) {
		return !topic.equalsIgnoreCase(event.subject);
	}

}
