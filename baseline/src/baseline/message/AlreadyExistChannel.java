package baseline.message;

public class AlreadyExistChannel extends Exception {
	private static final long serialVersionUID = -2530371667639870338L;

	public AlreadyExistChannel(String message, Throwable cause) {
		super(message + ": " + cause.getMessage());
	}

	public AlreadyExistChannel(String message) {
		super(message);
	}

	public AlreadyExistChannel(Throwable cause) {
		super(cause.getMessage());
	}
}
