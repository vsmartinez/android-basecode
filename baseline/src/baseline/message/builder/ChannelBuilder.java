package baseline.message.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import baseline.message.Channel;
import baseline.message.filter.Filter;

/**
 * Helper class to build a channel inline
 * 
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 * 
 */
public class ChannelBuilder {

	private String topic;
	private Pattern pattern;
	private List<Filter> filters;

	public ChannelBuilder() {
		filters = new ArrayList<Filter>();
	}

	public ChannelBuilder topic(String topic) {
		this.topic = topic;
		return this;
	}

	public ChannelBuilder pattern(Pattern pattern) {
		this.pattern = pattern;
		return this;
	}

	public ChannelBuilder add(Filter filter) {
		this.filters.add(filter);
		return this;
	}

	public Channel build() {
		Channel c = new Channel(pattern, topic);

		for (Filter f : filters) {
			c.addFilter(f);
		}

		return c;
	}
}
