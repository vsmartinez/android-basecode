package baseline.message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import baseline.message.event.Event;
import baseline.message.filter.Filter;

/**
 * <p>
 * Class which represent a communication pipe between publisher and subscriber.
 * <p>
 * Every event published on this channel will be propagated to the subscriber.
 * <p>
 * This class also provide method to add/remove filters, those filter will be
 * applied to each event published on this channel, if a filter fails the event
 * will be dropped.
 * <p>
 * An event will be published on the channel if event's topic match with
 * channel's topic and if event pass through the filter list.
 * 
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 * 
 */

public class Channel {

	private List<Filter> filters;

	/**
	 * Add filter to the current channel
	 * 
	 * @param filter
	 *            to add
	 */
	public void addFilter(Filter filter) {
		filters.add(filter);
	}

	/**
	 * Remove filter to the current channel
	 * 
	 * @param filter
	 *            to delete
	 */
	public void removeFilter(Filter filter) {
		filters.remove(filter);
	}

	/**
	 * Delete all filters registered into this channel.
	 */
	public void clearFilters() {
		filters.clear();
	}

	private String topic;
	private Pattern pattern;

	/**
	 * No particular behavior
	 */
	public static int NONE = 0x0;

	/**
	 * Channel will be destroyed as soon as it was used.
	 */
	public static int ONE_SHOT = 0x1;

	/**
	 * Default channel flag
	 */
	private int flag = NONE;

	/**
	 * Constructor of a channel, you must provide a {@link Pattern} which will
	 * be used to check if an event match with this channel, and a free text as
	 * a description about what this channel is for.
	 * 
	 * @param pattern
	 *            which every event must pass to be published on this channel.
	 * @param topic
	 *            Free text describing the meaning of this channel
	 */
	public Channel(Pattern pattern, String topic) {
		this(pattern, topic, NONE);
	}

	/**
	 * Constructor of a channel, you must provide a {@link Pattern} which will
	 * be used to check if an event match with this channel, and a free text as
	 * a description about what this channel is for.
	 * 
	 * @param pattern
	 *            which every event must pass to be published on this channel.
	 * @param topic
	 *            Free text describing the meaning of this channel
	 * @param flag
	 *            define the channels behavior. Available flags are
	 *            {@link Channel#NONE}, {@link Channel#ONE_SHOT}
	 */
	public Channel(Pattern pattern, String topic, int flag) {

		if (pattern == null) {
			throw new IllegalArgumentException("pattern can't be null");
		}

		if (topic == null) {
			throw new IllegalArgumentException("topic can't be null");
		}

		this.topic = topic;
		this.pattern = pattern;
		this.flag = flag;

		filters = Collections.synchronizedList(new ArrayList<Filter>());
	}

	/**
	 * Retrieve the channel's topic
	 * 
	 * @return string topic
	 */
	public String getTopic() {
		return topic;
	}

	/**
	 * <p>
	 * Check if an event must be dropped.
	 * <p>
	 * An event will be dropped if do not pass at least 1 of filters bound to
	 * channel.
	 * 
	 * 
	 * 
	 * @param event
	 *            to check if pass through the filter list.
	 * @return true if check, false otherwise.
	 */
	public boolean discard(Event event) {

		synchronized (filters) {
			boolean r = false;

			for (Filter filter : filters) {
				r |= filter.discard(topic, event);
				if (r) {
					break;
				}
			}
		}

		return false;
	}

	/**
	 * Method to know if a specific topic match with this channel
	 * 
	 * @param topic
	 *            to match
	 * @return true if match, false otherwise
	 */
	public boolean matchTopic(String topic) {
		return pattern.matcher(topic).matches();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (!(obj instanceof Pattern)) {
			return false;
		}

		Pattern p = (Pattern) obj;
		return p.pattern().equals(this.pattern);
	}

	@Override
	public int hashCode() {
		return pattern.hashCode();
	}

	/**
	 * String pattern associated to Channel
	 * 
	 * @return pattern string
	 */
	public String pattern() {
		return pattern.pattern();
	}

	public int getFlag() {
		return flag;
	}
}
