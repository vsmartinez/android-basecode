package baseline.message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;

import baseline.message.event.Event;
import baseline.platform.ConsoleLog;
import baseline.platform.Log;

/**
 * <p>
 * Main class to manage Channels and provide methods to publish events.
 * <p>
 * Messages published by {@link ChannelManager#publish(Event)} or
 * {@link ChannelManager#publish(List)} will be scheduled based on each events
 * priority.
 * <p>
 * In order to define if a Event must be send though a particular channel the
 * {@link Event#subject} will be matched against the {@link Channel#getTopic()}.
 * <p>
 * Default event order is natural order (higher first), but you can define your
 * own comparator. <b>see {@link ChannelManager#setComparator(Comparator)}</b>
 * <p>
 * You don't need to synchronize your code due ChannelManager's method are
 * already synchronized.
 * <p>
 * You must call {@link ChannelManager#start()} and
 * {@link ChannelManager#stop()} method to run and stop the class, if you don't
 * call to start method, ChannelManager will not work.
 * 
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 * 
 */
public class ChannelManager {

	volatile private boolean running = false;

	private PriorityBlockingQueue<Event> queue;

	// Default priority comparator
	private Comparator<Event> comparator = new Comparator<Event>() {

		@Override
		public int compare(Event e1, Event e2) {
			return e1.compareTo(e2);
		}
	};

	private int capacity = 10;

	private Log log;

	/**
	 * Default constructor with queue capacity set to 10 and natural order of
	 * event's priority, activity will be logged to console.
	 */
	public ChannelManager() {
		this(new ConsoleLog());
	}

	/**
	 * Construct a ChannelManager instance with queue capacity set to 10 and
	 * natural order of event's priority, and use log as logging instance.
	 * 
	 * 
	 * @param log
	 */
	public ChannelManager(Log log) {
		this.log = log;
	}

	/**
	 * Constructor which allow you to define queue capacity and natural order of
	 * event's priority and log instance.
	 * 
	 * @param capacity
	 *            queue capacity
	 */
	public ChannelManager(Log log, int capacity) {
		this.log = log;
		this.capacity = capacity;
	}

	/**
	 * Set comparator to be used to define queue order
	 * 
	 * @param comparator
	 *            user defined comparator
	 */
	public void setComparator(Comparator<Event> comparator) {
		this.comparator = comparator;
	}

	/**
	 * <p>
	 * Start the worker.
	 * <p>
	 * Successive call to this method will be ignored.
	 */
	public void start() {
		if (running) {
			return;
		}

		queue = new PriorityBlockingQueue<Event>(capacity, comparator);
		running = true;
		worker = new Thread(channelRunnable);
		worker.start();
	}

	private Thread worker;

	private Object lock = new Object();

	/**
	 * <p>
	 * Stop worker, no more events will be dispatched
	 * <p>
	 * Successive call to this method will be ignored.
	 */
	public void stop() {
		if (!running) {
			return;
		}

		running = false;

		synchronized (lock) {
			lock.notifyAll();
		}

		// Waiting to worker instance finish its work
		try {
			worker.join();
		} catch (InterruptedException e) {
			log.debug("Error joining worker at stop.", e);
		}

		worker = null;
		channelRunnable = null;

		synchronized (lock) {
			queue.clear();
		}
	}

	private Map<Channel, List<Subscriber>> map = Collections
			.synchronizedMap(new HashMap<Channel, List<Subscriber>>());

	/**
	 * <p>
	 * Register a subscribe to an specific channel.
	 * 
	 * @param channel
	 *            to subscribe the callback
	 * @param subscriber
	 *            callback to register
	 * 
	 */
	public void subscribe(Channel channel, Subscriber subscriber) {
		List<Subscriber> subs = map.get(channel);

		if (subs == null) {
			subs = new ArrayList<Subscriber>();
		}

		subs.add(subscriber);
		map.put(channel, subs);
	}

	/**
	 * <p>
	 * Register a callback on each channel which match
	 * {@link Channel#matchTopic(String)} with its topic.
	 * {@link Channel#getTopic()}
	 * <p>
	 * if topic doesn't match with any of the channel's topics an
	 * {@link NonExistingChannel} exception will be throw
	 * 
	 * @param topic
	 *            to match with channel's topic
	 * @param subscriber
	 *            callback to register
	 * @throws NonExistingChannel
	 *             thrown when topic doesn't match with any channel's topic
	 */
	public void subscribe(String topic, Subscriber subscriber)
			throws NonExistingChannel {
		Set<Channel> sc = map.keySet();

		boolean added = false;

		synchronized (map) {
			for (Channel channel : sc) {
				if (channel.matchTopic(topic)) {
					map.get(channel).add(subscriber);
					added = true;
				}
			}
		}

		if (!added) {
			throw new NonExistingChannel(String.format(
					"There is no channel matching with %s topic.", topic));
		}
	}

	/**
	 * <p>
	 * Add a channel to the list of available channels.
	 * <p>
	 * If a channel has same pattern an {@link AlreadyExistChannel} exception
	 * will be thrown.
	 * 
	 * @param channel
	 *            to add
	 * @throws AlreadyExistChannel
	 *             if channel patter already exist
	 */
	public void addChannel(Channel channel) throws AlreadyExistChannel {
		if (map.containsKey(channel)) {
			throw new AlreadyExistChannel(String.format(
					"Already exist a channel with same pattern %s.",
					channel.pattern()));
		}

		map.put(channel, new ArrayList<Subscriber>());
	}

	/**
	 * <p>
	 * Remove desired channel.
	 * <p>
	 * Successive event publishing to this channel will be discarded.
	 * 
	 * @param channel
	 *            to delete
	 */
	public void removeChannel(Channel channel) {
		synchronized (lock) {
			map.remove(channel);
		}
	}

	private Runnable channelRunnable = new Runnable() {

		@Override
		public void run() {

			synchronized (lock) {
				while (running) {
					while (queue.isEmpty() && running) {
						try {
							lock.wait();
						} catch (InterruptedException e) {
							// nothing to do
						}
					}

					if (!running) {
						return;
					}

					Event e = queue.poll();
					Set<Channel> sc = map.keySet();

					synchronized (map) {
						boolean notified = false;
						Iterator<Channel> i = sc.iterator();

						while (i.hasNext()) {

							Channel channel = i.next();

							if (!channel.matchTopic(e.subject)) {
								continue;
							}

							if (channel.discard(e)) {
								log.debug(String
										.format("Event with subject %s, priority %s, payload %s was dropped because do not pass at least one filter in channel %s",
												e.subject, e.priority,
												e.payload, channel.getTopic()));
								continue;
							}

							List<Subscriber> subscribers = map.get(channel);
							for (Subscriber subscriber : subscribers) {
								subscriber.notify(e.payload);
							}

							notified = true;

							// One shot channel, means once this channel was
							// used one time it will be destroyed.
							if ((channel.getFlag() & Channel.ONE_SHOT) > 0) {
								i.remove();
							}
						}

						if (!notified) {
							log.debug(String
									.format("Event with subject %s, priority %s, payload %s was dropped. There is not matching channel or doesn't pass the filters",
											e.subject, e.priority, e.payload));
						}
					}
				}
			}
		}
	};

	/**
	 * <p>
	 * Add an event to the queue to be scheduled and then published to the
	 * matching channels.
	 * <p>
	 * An event will be published if {@link Event#subject} match with any
	 * {@link Channel#getTopic()} and pass through all its filters. If not event
	 * will be discarded.
	 * 
	 * <p>
	 * Event published after call {@link ChannelManager#stop()} method will be
	 * discarded.
	 * 
	 * @param event
	 *            to notify
	 */
	public void publish(Event event) {

		if (!running) {
			return;
		}

		queue.add(event);

		synchronized (lock) {
			lock.notifyAll();
		}
	}

	/**
	 * <p>
	 * Add a list of events to the queue to be scheduled and then published to
	 * the matching channels.
	 * <p>
	 * An event will be published if {@link Event#subject} match with any
	 * {@link Channel#getTopic()} and pass through all its filters. If not event
	 * will be discarded.
	 * <p>
	 * List of events published after call {@link ChannelManager#stop()} method
	 * will be discarded.
	 * 
	 * @param events
	 *            to notify
	 */
	public void publish(List<Event> events) {

		if (!running) {
			return;
		}

		queue.addAll(events);

		synchronized (lock) {
			lock.notifyAll();
		}
	}

	/**
	 * Retrieve a list available channels
	 * 
	 * @return list of channels
	 */
	public List<Channel> getChannels() {
		Set<Channel> sc = map.keySet();

		if (sc == null) {
			return new ArrayList<Channel>();
		}

		return new ArrayList<Channel>(sc);
	}
}
