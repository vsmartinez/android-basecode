package baseline.message.event;

/**
 * <p>
 * Event class transport a Payload object through the {@link baseline.message.ChannelManager}.
 * All event as a priority to define its scheduling.
 * <p>
 * Default priority is 5 {@link Event#NORMAL}, lowest is 0 {@link Event#LOW},
 * highest 10 {@link Event#HIGH}, if you set the priority above the boundaries
 * it will be cut to nearest priority {@link Event#LOW}, {@link Event#HIGH}
 * based on your priority value.
 * 
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class Event implements Comparable<Event> {

	public static int LOW = 0;
	public static int HIGH = 10;
	public static int NORMAL = 5;

	public Object payload;

	public String subject;

	public int priority;

	/**
	 * <p>
	 * Construct an event with {@link Event#NORMAL} priority.
	 * 
	 * @param subject
	 *            free text to match against the channels
	 * @param payload
	 *            object to transport
	 */
	public Event(String subject, Object payload) {
		this(subject, payload, NORMAL);
	}

	/**
	 * 
	 * <p>
	 * Construct an event
	 * 
	 * @param subject
	 *            free text to match against the channels and its filters
	 * @param payload
	 *            object to transport
	 * @param priority
	 *            Helper properties {@link Event#LOW}, {@link Event#NORMAL},
	 *            {@link Event#HIGH}. Priority above {@link Event#HIGH} will be
	 *            cut to {@link Event#HIGH}. Priority below {@link Event#LOW}
	 *            will be cut to {@link Event#LOW}
	 * 
	 */
	public Event(String subject, Object payload, int priority) {
		this.payload = payload;
		this.subject = subject;

		if (priority > 10) {
			this.priority = 10;
		}

		if (priority < 0) {
			this.priority = 0;
		}

		this.priority = priority;
	}

	@Override
	public int compareTo(Event e) {
		return e.priority - priority;
	}
}
