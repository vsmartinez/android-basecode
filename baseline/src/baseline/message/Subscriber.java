package baseline.message;

/**
 * Interface to register a callback into {@link ChannelManager} class.
 * 
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>  
 */
public interface Subscriber {

	/**
	 * Method to be called when something happens on the subscribed channel.
	 * 
	 * @param payload
	 *            Any object to share with the subscriber
	 */
	 void notify(Object payload);
}
