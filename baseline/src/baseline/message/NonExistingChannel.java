package baseline.message;

public class NonExistingChannel extends Exception {

	private static final long serialVersionUID = -5488341549140708526L;

	public NonExistingChannel(String message, Throwable cause) {
		super(message + ": " + cause.getMessage());
	}

	public NonExistingChannel(String message) {
		super(message);
	}

	public NonExistingChannel(Throwable cause) {
		super(cause.getMessage());
	}
}
