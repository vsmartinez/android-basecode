package baseline.storage;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class StorageException extends Exception {

	private static final long serialVersionUID = -5319287422547689519L;

	public StorageException() {
		super();
	}

	public StorageException(String message, Throwable cause) {
		super(message + ": " + cause.getMessage());
	}

	public StorageException(String message) {
		super(message);
	}

	public StorageException(Throwable cause) {
		super(cause.getMessage());
	}

}
