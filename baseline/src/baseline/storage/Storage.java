package baseline.storage;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public interface Storage {

    // -------------------------------------------------------------------------
    // Transactions methods.
    // -------------------------------------------------------------------------

    /**
     * All method in {@link Storage} must be executed into a transaction instance.
     * This must be applied for both read and write transaction.
     */
    abstract class Transaction {

        /**
         * Flag current transacction as successful.
         * If this method is not called then {@link Storage#endTransaction(Transaction)} will rollback the whole transaction.         */
        public abstract void success();

    }

    /**
     * Start a new transaction as read only based on its readOnly parameter.
     *
     * @param readOnly true to flat current transaction as read only, false otherwise.
     */
    Transaction startTransaction(boolean readOnly);

    /**
     * Start a new transaction as read only.
     */
    Transaction startTransaction();

    /**
     * Transaction will be closed. Transactions flagged as success will store its data into the underlying storage or rollback otherwise.
     */
    void endTransaction(Transaction transaction);

    // -------------------------------------------------------------------------
    // CRUD methods one by one for each table
    // -------------------------------------------------------------------------

    // void insert(Transaction t, String data) throws StorageException;
    //
    // void update(Transaction t, String data) throws StorageException;
}