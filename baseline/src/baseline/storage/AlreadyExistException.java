package baseline.storage;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class AlreadyExistException extends StorageException {

	private static final long serialVersionUID = -7960254697712651735L;

	public AlreadyExistException() {
		super();
	}

	public AlreadyExistException(String message, Throwable cause) {
		super(message + ": " + cause.getMessage());
	}

	public AlreadyExistException(String message) {
		super(message);
	}

	public AlreadyExistException(Throwable cause) {
		super(cause.getMessage());
	}

}
