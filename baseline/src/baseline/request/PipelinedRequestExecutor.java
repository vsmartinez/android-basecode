package baseline.request;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import baseline.message.AlreadyExistChannel;
import baseline.message.Channel;
import baseline.message.ChannelManager;
import baseline.message.Subscriber;
import baseline.message.event.Event;
import baseline.request.RequestExecutor.ExecutorListener;

/**
 * <p>
 * The goal of this class is provide a priority queue of {@Request}
 * instances to be executed by {@RequestExector}.
 * <p>
 * Due the asynchronous nature of {@ChannelManager} you must
 * provide an {@ExecutorListener} instance as a callback in
 * order to be notified as soon as {@RequestExecturo} have
 * done with the request.
 * <p>
 * Since {@link PipelinedRequestExecutor} as a {@ChannelManage}
 * underneath, you must {@link PipelinedRequestExecutor#start()} and
 * {@link PipelinedRequestExecutor#stop()} instances of this class, if you don't
 * follow this, dispatching events wont' work.
 * 
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 * 
 */
public class PipelinedRequestExecutor {

	private RequestExecutor re;

	private ChannelManager cm;

	/**
	 * Two arguments are needed {@RequestExectuor} and
	 * {@ChannelManager}
	 * 
	 * @param requestExecutor
	 *            a {@RequestExectuor} instance
	 * @param channelManager
	 *            a {@ChannelManager} instance
	 */
	public PipelinedRequestExecutor(RequestExecutor requestExecutor,
			ChannelManager channelManager) {
		if (requestExecutor == null) {
			throw new IllegalArgumentException(
					"Request Executor can't be null.");
		}

		if (channelManager == null) {
			throw new IllegalArgumentException("Channel Manager can't be null.");
		}

		re = requestExecutor;
		cm = channelManager;
	}

	/**
	 * Start underlining {@link ChannelManager} instance.
	 */
	public void start() {
		cm.start();
	}

	/**
	 * Stop underlining {@link ChannelManager} instance.
	 */
	public void stop() {
		cm.stop();
	}

	/**
	 * <p>
	 * Put the {@link Request} into the queue with normal priority to be execute
	 * by the scheduler, then callback will be called with requests result
	 * object
	 * 
	 * @param request
	 *            to be enqueued and the executed based its priority
	 * @param callback
	 *            to notify about the result of executed request
	 */
	public <T> void publish(final Request<T> request,
			final ExecutorListener<T, RequestException> callback) {
		publish(request, Event.NORMAL, callback);
	}

	/**
	 * <p>
	 * Put the {@link Request} into the queue with desired priority to be
	 * execute by the scheduler, then callback will be called with requests
	 * result object
	 * 
	 * @param request
	 *            to be enqueued and the executed based its priority
	 * @param priority
	 *            to be used by the scheduler
	 * @param callback
	 *            to notify about the result of executed request
	 */
	public <T> void publish(final Request<T> request, int priority,
			final ExecutorListener<T, RequestException> callback) {

		// Private channel ID
		final String localPattern = "ONE_SHOT_CHANNEL_" + UUID.randomUUID();

		final Channel channel = new Channel(Pattern.compile(localPattern),
				"Private channel, only one event will use this channel.",
				Channel.ONE_SHOT);

		try {
			cm.addChannel(channel);
		} catch (AlreadyExistChannel e) {
			// Shouln't happen, but if so, we can use the existing one.
		}

		cm.subscribe(channel, new Subscriber() {

			// Funny: payload parameter is useless
			@Override
			public void notify(Object payload) {
				try {
					T t = re.sync(request);
					callback.succeed(t);
				} catch (RequestException e) {
					callback.failure(e);
				}
			}
		});

		// Adding event to the queue
		cm.publish(new Event(localPattern, request, priority));

	}

	/**
	 * <p>
	 * Put a list of {@link Request}s with same generic type into the queue with
	 * normal priority to be execute by the scheduler, then callback will be
	 * called with requests result object
	 * 
	 * @param request
	 *            to be enqueued and the executed based its priority
	 * @param callback
	 *            to notify about the result of executed request
	 */
	public <T> void publish(final List<Request<T>> request,
			final ExecutorListener<T, RequestException> callback) {
		publish(request, Event.NORMAL, callback);
	}

	/**
	 * <p>
	 * Put a list of {@link Request}s with same generic type into the queue with
	 * desired priority to be execute by the scheduler, then callback will be
	 * called with requests result object.
	 * <p>
	 * All requests will be executed with same priority
	 * 
	 * @param requests
	 *            to be enqueued and the executed based its priority
	 * @param priority
	 *            to be used by the scheduler
	 * @param callback
	 *            to notify about the result of executed request
	 */
	public <T> void publish(final List<Request<T>> requests, int priority,
			final ExecutorListener<T, RequestException> callback) {

		// Private channel ID
		final String localPattern = "LIST_CHANNEL_" + UUID.randomUUID();

		final Channel channel = new Channel(
				Pattern.compile(localPattern),
				"Private channel, a list of event with same payload type will use this channel.");

		try {
			cm.addChannel(channel);
		} catch (AlreadyExistChannel e) {
			// Shouln't happen, but if so, we can use the existing one.
		}

		cm.subscribe(channel, new Subscriber() {

			@SuppressWarnings("unchecked")
			@Override
			public void notify(Object payload) {
				try {
					T t = re.sync((Request<T>) payload);
					callback.succeed(t);
				} catch (RequestException e) {
					callback.failure(e);
				}
			}
		});

		// Adding event to the queue
		List<Event> events = new ArrayList<Event>();
		for (Request<T> request : requests) {
			events.add(new Event(localPattern, request, priority));
		}

		cm.publish(events);
	}
}
