package baseline.request;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import json.parser.JsonParser;
import json.parser.JsonParser.JsonTransformer;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public abstract class JsonBaseRequest<T> extends BaseRequest<T> {

	static {
		JsonParser.addTransformer(Date.class, new JsonTransformer<Date>() {

			@Override
			public Date reader(String value) {

				if (value == null) {
					return null;
				}

				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"yyyy-MM-dd'T'hh:mm:ssZ");
				try {
					return dateFormat.parse(value);
				} catch (ParseException e) {
					return null;
				}
			}

			@Override
			public String writer(Date value) {

				if (value == null) {
					return null;
				}

				return value.toString();
			}

		});
	}

	@Override
	public String getContentType() {
		return "application/json";
	}

	@Override
	public String getAcceptType() {
		return "application/json";
	}

}
