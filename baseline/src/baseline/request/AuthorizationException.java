package baseline.request;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class AuthorizationException extends RequestException {

	private static final long serialVersionUID = -4822318740969496626L;

	public AuthorizationException() {
		super();
	}

	public AuthorizationException(String message, Throwable cause) {
		super(message, cause);
	}

	public AuthorizationException(String message) {
		super(message);
	}

	public AuthorizationException(Throwable cause) {
		super(cause);
	}
}
