package baseline.request.proxymock;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

import baseline.request.Request;

public class ProxyMock {

	private Map<String, Object> map = new HashMap<String, Object>();

	private Method method;

	@SuppressWarnings("unchecked")
	public <T> T mock(final Class<T> clazz) {

		Object o = Proxy.newProxyInstance(clazz.getClassLoader(),
				new Class[] { Request.class }, new InvocationHandler() {

					@Override
					public Object invoke(Object paramObject,
							Method paramMethod, Object[] paramArrayOfObject)
							throws Throwable {

						Object ob = map.get(paramMethod.toString());

						if (ob != null) {

							if (ob instanceof Throwable) {
								throw ((Throwable) ob);
							}

							return ob;
						}

						method = paramMethod;

						return returnDefault(paramMethod.getReturnType());
					}
				});

		return (T) o;
	}

	private static Object returnDefault(Class<?> type) {

		if (!type.isPrimitive()) {
			return null;
		}

		if (type.isAssignableFrom(int.class)
				|| type.isAssignableFrom(float.class)
				|| type.isAssignableFrom(long.class)
				|| type.isAssignableFrom(double.class)
				|| type.isAssignableFrom(byte.class)) {
			return 0;
		}

		if (type.isAssignableFrom(boolean.class)) {
			return false;
		}

		return null; // Should never reach here
	}

	public static interface OngoingStubbing<E> {
		void thenReturn(E value);

		void thenThrowException(Throwable e);
	}

	public <E, T> OngoingStubbing<E> when(final T methodCall) {
		return new OngoingStubbing<E>() {

			@Override
			public void thenReturn(E value) {
				map.put(method.toString(), value);
				method = null;
			}

			@Override
			public void thenThrowException(Throwable e) {
				map.put(method.toString(), e);
				method = null;
			}

		};
	}
}
