package baseline.request;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

import baseline.utils.IOUtils;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class Response {

	private final HttpURLConnection connection;
	private final Request<?> request;
	
	public Response(HttpURLConnection connection, Request<?> request) {
		this.request = request;
		this.connection = connection;
	}

	public URL getUrl(){
		return connection.getURL();
	}
	
	public Request<?> getRequest(){
		return request;
	}
	
	public Reader asReader() throws IOException {
		return asReader(Charset.defaultCharset());
	}

	public Reader asReader(Charset charset) throws IOException {

		try {
			String enc = connection.getContentEncoding();
			if (enc != null)
				charset = Charset.forName(enc);
		} catch (Exception e) {
			e.printStackTrace();
		}

		InputStream stream = connection.getResponseCode() == 200 ? connection
				.getInputStream() : connection.getErrorStream();

		return new BufferedReader(new InputStreamReader(stream, charset) {

			@Override
			public void close() throws IOException {
				super.close();
				connection.disconnect();
			}

		});
	}

	public InputStream asStream() throws IOException {

		InputStream stream = connection.getResponseCode() == 200 ? connection
				.getInputStream() : connection.getErrorStream();

		return new BufferedInputStream(stream) {

			@Override
			public void close() throws IOException {
				super.close();
				connection.disconnect();
			}

		};
	}

	public String getCookie() {
		String cookie = connection.getHeaderField("Set-Cookie");
		if (cookie == null)
			return null;
		return cookie.substring(0, cookie.indexOf(';'));
	}

	public int getStatusCode() throws IOException {
		return connection.getResponseCode();
	}

	public Integer getContentLength() {
		int cl = connection.getContentLength();
		if (cl == -1)
			return null;
		return cl;
	}

	public String asString() throws IOException {
		return asString(Charset.defaultCharset());
	}

	public String asString(Charset charset) throws IOException {

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		BufferedReader r = null;
		try {
			r = new BufferedReader(asReader(charset));
			String s;
			while ((s = r.readLine()) != null)
				pw.println(s);

		} finally {
			pw.flush();
			IOUtils.close(pw);
			IOUtils.close(r);
		}

		return sw.toString();
	}
}