package baseline.request;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import baseline.cache.CacheRequest;
import baseline.platform.ConsoleLog;
import baseline.platform.Log;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class RequestExecutor {

    private static int MAX_POOL = 5;
    private int networkLimit = MAX_POOL;

    private ExecutorService pool = Executors.newFixedThreadPool(networkLimit);

    protected CacheRequest cacheRequest;

    public interface ExecutorListener<T, E extends RequestException> {

        /**
         * Inform that the request has ended properly
         */
        void succeed(T t);

        /**
         * Inform that there has been an error while executing this request
         */
        void failure(E e);
    }

    private Log log;

    public RequestExecutor(CacheRequest cacheRequest) {
        this(cacheRequest, new ConsoleLog());
    }

    public RequestExecutor(CacheRequest cacheRequest, Log log) {
        this.log = log;
        this.cacheRequest = cacheRequest;
    }

    /**
     * Allow to change how many thread can fit into the executor pool at runtime.
     *
     * @param networkLimit number of thread into the pool
     */
    public void setNetworkLimit(int networkLimit) {
        this.networkLimit = networkLimit;
        pool.shutdown(); // finishing existing threads
        pool = Executors.newFixedThreadPool(networkLimit);
    }

    /**
     * Returns the cached data of a the passed request or executes the request
     * in the current thread if the data doesn't exists
     *
     * @param request
     * @return
     * @throws RequestException
     */
    public <T> T sync(final Request<T> request) throws RequestException {
        return cacheRequest.get(request);
    }

    /**
     * Executes the passed requests in the current thread.
     *
     * @param request
     * @return
     * @throws RequestException
     */
    public <T> T force(final Request<T> request) throws RequestException {
        log.info(String.format("RequestExecutor request: %s", request));

        return cacheRequest.force(request);
    }

    /**
     * Executes the passed requests in the current thread.
     *
     * @param request
     * @return
     */
    public <T> void async(final Request<T> request) {
        async(request, null);
    }

    /**
     * Returns the cached data of a the passed request or executes the request
     * in a new thread if the data doesn't exists. Data is passed to the
     * ExecutorListener.
     *
     * @param request
     * @return
     */
    public <T> void async(final Request<T> request,
                          final ExecutorListener<T, ? super RequestException> executorListener) {

        final Future<T> f = pool.submit(new Callable<T>() {

            @Override
            public T call() throws Exception {
                return sync(request);
            }
        });

        if (executorListener == null) {
            return;
        }

        new Thread() {
            @Override
            public void run() {

                try {
                    executorListener.succeed(f.get());
                } catch (InterruptedException e) {
                    executorListener.failure(new RequestException(e));
                } catch (ExecutionException e) {
                    executorListener.failure(new RequestException(e));
                }
            }
        }.start();
    }

    /**
     * Executes the request in a new thread. Data is passed to the
     * ExecutorListener.
     *
     * @param request
     * @return
     */
    public <T> void forceAsync(final Request<T> request) {
        forceAsync(request, null);
    }

    /**
     * Executes the request in a new thread. Data is passed to the
     * ExecutorListener.
     *
     * @param request
     * @return
     */
    public <T> void forceAsync(
            final Request<T> request,
            final ExecutorListener<T, ? super RequestException> executorListener) {

        final Future<T> f = pool.submit(new Callable<T>() {

            @Override
            public T call() throws Exception {
                return force(request);
            }
        });

        if (executorListener == null) {
            return;
        }

        new Thread() {
            @Override
            public void run() {

                try {
                    executorListener.succeed(f.get());
                } catch (InterruptedException e) {
                    executorListener.failure(new RequestException(e));
                } catch (ExecutionException e) {
                    executorListener.failure(new RequestException(e));
                }
            }
        }.start();
    }

}
