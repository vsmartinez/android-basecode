package baseline.request;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class RedirectException extends RequestException {

	private static final long serialVersionUID = 3709451445130123123L;

	public RedirectException() {
		super();
	}

	public RedirectException(String message, Throwable cause) {
		super(message, cause);
	}

	public RedirectException(String message) {
		super(message);
	}

	public RedirectException(Throwable cause) {
		super(cause);
	}
}
