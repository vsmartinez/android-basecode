package baseline.request;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class RequestManager {
	public String url;
	private Proxy proxy;

	public RequestManager(String url) {
		this(url, null);
	}

	public RequestManager(String url, Proxy proxy) {
		this.url = url;
		this.proxy = proxy;
	}

	private String cookie = null;

	public static class ParameterBuilder {
		private Map<String, String> parameters;

		public ParameterBuilder() {
			parameters = new LinkedHashMap<String, String>();
		}

		public ParameterBuilder add(String key, String value) {
			parameters.put(key, value);
			return this;
		}

		public ParameterBuilder add(String key, int value) {
			return add(key, String.valueOf(value));
		}

		public ParameterBuilder add(String key, boolean value) {
			return add(key, String.valueOf(value));
		}

		public ParameterBuilder add(String key, long value) {
			return add(key, String.valueOf(value));
		}

		public ParameterBuilder add(String key, float value) {
			return add(key, String.valueOf(value));
		}

		public ParameterBuilder add(String key, double value) {
			return add(key, String.valueOf(value));
		}

		public int size() {
			return parameters.size();
		}

		public String build() {
			String params = "";

			for (String key : parameters.keySet()) {
				try {
					params += "&" + URLEncoder.encode(key, "UTF-8") + "="
							+ URLEncoder.encode(parameters.get(key), "UTF-8");

				} catch (UnsupportedEncodingException e) {
					// Nothing to do
				}
			}

			params = params.substring(1);
			return params;
		}
	}

	// POST
	public Response doPost(Request<?> request) throws Exception {

		StringBuilder str = new StringBuilder();

		addPath(str, request.getPath());

		ParameterBuilder p = new ParameterBuilder();
		request.writeParameter(p);
		addParameters(str, p);

		URL url = URI.create(str.toString()).toURL();

		HttpURLConnection con = (HttpURLConnection) (proxy != null ? url
				.openConnection(proxy) : url.openConnection());
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestMethod("POST");
		con.setConnectTimeout(1000);

		con.setRequestProperty("Accept", request.getAcceptType());
		con.setRequestProperty("Content-type", request.getContentType());

		if (cookie != null)
			con.setRequestProperty("Cookie", cookie);

		OutputStream out = null;
		try {
			out = con.getOutputStream();
			request.writeBody(out);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
		}

		cookie = con.getRequestProperty("Set-Cookie");
		if (cookie != null) {
			cookie = cookie.substring(0, cookie.indexOf(";"));
		}

		return new Response(con, request);
	}

	// PUT
	public Response doPut(Request<?> request) throws Exception {

		StringBuilder str = new StringBuilder();

		addPath(str, request.getPath());

		ParameterBuilder p = new ParameterBuilder();
		request.writeParameter(p);
		addParameters(str, p);

		URL url = URI.create(str.toString()).toURL();
		HttpURLConnection con = (HttpURLConnection) (proxy != null ? url
				.openConnection(proxy) : url.openConnection());
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestMethod("PUT");
		con.setConnectTimeout(1000);

		con.setRequestProperty("Accept", request.getAcceptType());
		con.setRequestProperty("Content-type", request.getContentType());

		if (cookie != null)
			con.setRequestProperty("Cookie", cookie);

		OutputStream out = null;
		try {
			out = con.getOutputStream();
			request.writeBody(out);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
		}

		cookie = con.getRequestProperty("Set-Cookie");
		if (cookie != null) {
			cookie = cookie.substring(0, cookie.indexOf(";"));
		}

		return new Response(con, request);
	}

	// GET
	public Response doGet(Request<?> request) throws Exception {
		StringBuilder str = new StringBuilder();

		addPath(str, request.getPath());

		ParameterBuilder p = new ParameterBuilder();
		request.writeParameter(p);
		addParameters(str, p);

		URL url = new URL(str.toString());
		HttpURLConnection con = (HttpURLConnection) (proxy != null ? url
				.openConnection(proxy) : url.openConnection());
		con.setDoInput(true);
		con.setRequestMethod("GET");
		con.setConnectTimeout(1000);
		con.setChunkedStreamingMode(0);

		con.setRequestProperty("Accept", request.getAcceptType());
		con.setRequestProperty("Content-type", request.getContentType());

		if (cookie != null)
			con.setRequestProperty("Cookie", cookie);

		con.connect();

		cookie = con.getHeaderField("Set-Cookie");
		if (cookie != null) {
			cookie = cookie.substring(0, cookie.indexOf(";"));
		}

		return new Response(con, request);
	}

	// DELETE
	public Response doDelete(Request<?> request) throws Exception {
		StringBuilder str = new StringBuilder();

		addPath(str, request.getPath());

		ParameterBuilder p = new ParameterBuilder();
		request.writeParameter(p);
		addParameters(str, p);

		URL url = new URL(str.toString());
		HttpURLConnection con = (HttpURLConnection) (proxy != null ? url
				.openConnection(proxy) : url.openConnection());
		con.setDoInput(true);
		con.setRequestMethod("DELETE");
		con.setConnectTimeout(1000);
		con.setChunkedStreamingMode(0);

		con.setRequestProperty("Accept", request.getAcceptType());
		con.setRequestProperty("Content-type", request.getContentType());

		if (cookie != null)
			con.setRequestProperty("Cookie", cookie);

		con.connect();

		cookie = con.getHeaderField("Set-Cookie");
		if (cookie != null) {
			cookie = cookie.substring(0, cookie.indexOf(";"));
		}

		return new Response(con, request);
	}

	private void addPath(StringBuilder str, String path) {
		if (path.startsWith("http://") || path.startsWith("https://")) {
			str.append(path);
			return;
		}

		str.append(url).append(path);
	}

	private static void addParameters(StringBuilder str, ParameterBuilder params) {
		if (params == null || params.size() == 0) {
			return;
		}

		str.append("?").append(params.build());
	}
}
