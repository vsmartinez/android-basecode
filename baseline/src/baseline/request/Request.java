package baseline.request;

import java.io.IOException;
import java.io.OutputStream;

import baseline.request.RequestManager.ParameterBuilder;

public interface Request<T> {

	/**
	 * HTTP method
	 */
	enum HttpMethod {
		GET, POST, PUT, DELETE;
	}

    /**
     * Transform Response object to a T instance
     *
     * @param response
     * @return T instance
     * @throws IOException
     */
    T processContent(Response response) throws Exception;

    String getPath();

	HttpMethod getMethod();

	String getContentType();

	String getAcceptType();

	void writeParameter(ParameterBuilder parameters);

	void writeBody(OutputStream out) throws Exception;

	T execute() throws RequestException;
	
}