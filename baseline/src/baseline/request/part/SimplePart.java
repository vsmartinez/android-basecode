package baseline.request.part;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class SimplePart extends Part {

	private String value;

	public SimplePart(String value) {
		this.value = value;
	}

	public SimplePart(int value) {
		this.value = String.valueOf(value);
	}

	public SimplePart(boolean value) {
		this.value = String.valueOf(value);
	}

	public SimplePart(float value) {
		this.value = String.valueOf(value);
	}

	public SimplePart(double value) {
		this.value = String.valueOf(value);
	}

	public SimplePart(long value) {
		this.value = String.valueOf(value);
	}

	@Override
	public void write(OutputStream out) throws IOException {
		out.write(value.getBytes());
	}
}
