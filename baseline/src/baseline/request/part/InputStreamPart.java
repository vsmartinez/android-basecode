package baseline.request.part;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class InputStreamPart extends Part {

	private InputStream is;
	private MimeType contentType;

	public InputStreamPart(InputStream inputStream, MimeType contentType) {
		this.is = inputStream;
		this.contentType = contentType;
	}

	@Override
	public MimeType getContentType() {
		return contentType;
	}

	@Override
	public void write(OutputStream out) throws IOException {
		byte[] buf = new byte[512];
		int i;

		while ((i = is.read(buf, 0, buf.length)) != -1) {
			out.write(buf, 0, i);
		}
		
		is.close();
	}

}
