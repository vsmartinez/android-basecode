package baseline.request.part.builder;

import baseline.request.part.MultiPart;
import baseline.request.part.MultiPart.ContentDisposition;
import baseline.request.part.Part;

public class MultiPartBuilder {

	private MultiPart mp;

	public MultiPartBuilder(String boundary,
			ContentDisposition contentDisposition) {
		mp = new MultiPart(boundary, contentDisposition);
	}

	public MultiPartBuilder addPart(Part part) {
		mp.addPart(part);
		return this;
	}

	public MultiPartBuilder addParameter(String name, String value) {
		mp.addParameter(name, value);
		return this;
	}

	public MultiPart build() {
		return mp;
	}

}
