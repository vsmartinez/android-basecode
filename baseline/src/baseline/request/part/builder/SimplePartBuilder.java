package baseline.request.part.builder;

import baseline.request.part.SimplePart;

public class SimplePartBuilder {

	private SimplePart sp;

	public SimplePartBuilder(String value) {
		sp = new SimplePart(value);
	}

	public SimplePartBuilder(int value) {
		sp = new SimplePart(value);
	}

	public SimplePartBuilder(boolean value) {
		sp = new SimplePart(value);
	}

	public SimplePartBuilder(float value) {
		sp = new SimplePart(value);
	}

	public SimplePartBuilder(double value) {
		sp = new SimplePart(value);
	}

	public SimplePartBuilder(long value) {
		sp = new SimplePart(value);
	}

	public SimplePartBuilder addParameter(String name, String value) {
		sp.addParameter(name, value);
		return this;
	}

	public SimplePart build() {
		return sp;
	}
}
