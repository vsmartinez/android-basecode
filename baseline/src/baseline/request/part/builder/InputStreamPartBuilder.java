package baseline.request.part.builder;

import java.io.InputStream;

import baseline.request.part.InputStreamPart;
import baseline.request.part.MimeType;

public class InputStreamPartBuilder {

	private InputStreamPart isp;

	public InputStreamPartBuilder(InputStream inputStream, MimeType contentType) {
		isp = new InputStreamPart(inputStream, contentType);
	}

	public InputStreamPartBuilder addParameter(String name, String value) {
		isp.addParameter(name, value);
		return this;
	}

	public InputStreamPart build() {
		return isp;
	}

}
