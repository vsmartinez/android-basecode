package baseline.request.part;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public abstract class Part {

	protected HashMap<String, String> parameters = new HashMap<String, String>();

	public void addParameter(String name, String value) {
		parameters.put(name, value);
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public abstract void write(OutputStream out) throws IOException;

	public MimeType getContentType() {
		return null;
	}
}
