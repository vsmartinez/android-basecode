package baseline.request.part;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class MultiPart extends Part {

	public enum ContentDisposition {
		FORM_DATA("form-data"), FILE("file");

		String content;

		ContentDisposition(String contentDisposition) {
			content = contentDisposition;
		}

		public String getContentDisposition() {
			return content;
		}
	}

	private static String CRLF = "\r\n";
	private static String TWO_HYPHENS = "--";
	private String boundary;
	private List<Part> parts;
	private ContentDisposition contentDisposition;

	public MultiPart(String boundary, ContentDisposition contentDisposition) {
		this.boundary = boundary;
		this.contentDisposition = contentDisposition;
		this.parts = new LinkedList<Part>();
	}

	public void addPart(Part part) {
		parts.add(part);
	}

	@Override
	public void write(OutputStream out) throws IOException {

		DataOutputStream dout = new DataOutputStream(out);

		dout.writeBytes(TWO_HYPHENS + boundary + CRLF);

		for (Part p : parts) {

			dout.writeBytes("Content-Disposition: ");
			dout.writeBytes(contentDisposition.getContentDisposition());
			writeParameters(dout, p.getParameters());
			dout.writeBytes(CRLF);

			writeContentType(dout, p.getContentType());

			dout.writeBytes(CRLF);
			p.write(dout);
			dout.writeBytes(CRLF);
		}

		dout.writeBytes(TWO_HYPHENS + boundary + TWO_HYPHENS + CRLF);
	}

	private static void writeParameters(DataOutputStream out,
			Map<String, String> parameters) throws IOException {
		for (String key : parameters.keySet()) {
			out.writeBytes("; ");
			out.writeBytes(key);
			out.writeBytes("=\"");
			out.writeBytes(parameters.get(key));
			out.writeBytes("\"");
		}
	}

	private static void writeContentType(DataOutputStream out,
			MimeType contentType) throws IOException {

		if (contentType == null) {
			return;
		}

		out.writeBytes("Content-Type: ");
		out.writeBytes(contentType.contentType);
		out.writeBytes(CRLF);
	}
}
