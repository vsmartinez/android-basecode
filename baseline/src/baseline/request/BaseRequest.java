package baseline.request;

import baseline.request.RequestManager.ParameterBuilder;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago Martinez</a>
 */
public abstract class BaseRequest<T> implements Request<T> {

    private static RequestManager requestManager;

    public static void setRequestManager(RequestManager requestManager) {
        BaseRequest.requestManager = requestManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see baseline.request.Request#processContent()
     */
    @Override
    public abstract T processContent(Response response) throws Exception;

    /*
     * (non-Javadoc)
     *
     * @see baseline.request.Request#getPath()
     */
    @Override
    public abstract String getPath();

    /*
     * (non-Javadoc)
     *
     * @see baseline.request.Request#getMethod()
     */
    @Override
    public abstract HttpMethod getMethod();

    /*
     * (non-Javadoc)
     *
     * @see baseline.request.Request#getContentType()
     */
    @Override
    public abstract String getContentType();

    /*
     * (non-Javadoc)
     *
     * @see baseline.request.Request#getAcceptType()
     */
    @Override
    public abstract String getAcceptType();

    /*
     * (non-Javadoc)
     *
     * @see baseline.request.Request#writeParameter(baseline.request.RequestManager.ParameterBuilder)
     */
    @Override
    public void writeParameter(ParameterBuilder parameters) {
        /**
         * Class which extends BaseRequest must fill this methods based on its needs.
         *
         * ParameterBuilder instances provide build method to help with this process
         */
        return;
    }

    /*
     * (non-Javadoc)
     *
     * @see baseline.request.Request#writeBody(java.io.OutputStream)
     */
    @Override
    public void writeBody(OutputStream out) throws Exception {
        /**
         * Class which extends BaseRequest must fill this methods based on its needs.
         *
         * This method will be used with POST request, for instances form-multiparrt
         */
        return;
    }

    /*
     * (non-Javadoc)
     *
     * @see baseline.request.Request#execute()
     */
    @Override
    public T execute() throws RequestException {
        Response response = null;
        try {
            if (requestManager == null)
                throw new RequestException("You must be logged before use this method");

            switch (getMethod()) {
                case GET:
                    response = requestManager.doGet(this);
                    break;

                case POST:
                    response = requestManager.doPost(this);
                    break;

                case PUT:
                    response = requestManager.doPut(this);
                    break;

                case DELETE:
                    response = requestManager.doDelete(this);

                default: // GET by default
                    response = requestManager.doGet(this);
                    break;
            }

            checkStatusAndThrow(response);

			/*
             * Only response (Success 2xx) with status code 200, 201, 202, 203, 204 will reach here.
			 */
            return processContent(response);

        } catch (IOException io) {
            if (response == null) {
                throw new RequestException(io);
            }

            try {
                checkStatusAndThrow(response);
                return null;
            } catch (IOException e) {
                throw new RequestException(e);
            }
        } catch (AuthorizationException ae) {
            throw ae;
        } catch (Exception e) {
            throw new RequestException(e);
        }
    }

    public void checkStatusAndThrow(Response response) throws RequestException, IOException {
        /*
         * Response Redirection 3xx
		 */
        if (response.getStatusCode() >= 300 && response.getStatusCode() < 400) {
            throw new RedirectException(response.asString());
        }

		/*
         * Response Error 4xx
		 */
        if (response.getStatusCode() >= 400 && response.getStatusCode() < 500) {
            throw new AuthorizationException(response.asString());
        }

		/*
         * Response Server Error 5xx
		 */
        if (response.getStatusCode() >= 500 && response.getStatusCode() < 600) {
            throw new RequestException(response.asString());
        }
    }

}
