package baseline.cache;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public interface CacheStorage {

	class CacheElement {

		private Object object;
		private long time;

		public CacheElement(Object object) {
			this(object, System.currentTimeMillis());
		}

		public CacheElement(Object object, long time) {
			this.object = object;
			this.time = time;
		}

		public synchronized void reset() {
			time = System.currentTimeMillis();
		}

		public synchronized Object get() {
			time = System.currentTimeMillis();
			return object;
		}

		public synchronized long getTime() {
			return time;
		}
	}

	CacheElement get(String key);

	void put(String key, CacheElement value);

	void clear();
}
