package baseline.cache;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 */
public class InMemoryCacheStorage implements CacheStorage {

	private Map<String, CacheElement> cache;

	public InMemoryCacheStorage() {
		cache = Collections
				.synchronizedMap(new LinkedHashMap<String, CacheElement>());
	}

	@Override
	public CacheElement get(String key) {
		return cache.get(key);
	}

	@Override
	public void put(String key, CacheElement value) {
		cache.put(key, value);
	}

	@Override
	public void clear() {
		cache.clear();
	}

}
