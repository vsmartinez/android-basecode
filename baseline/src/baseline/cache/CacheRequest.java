package baseline.cache;

import baseline.cache.CacheStorage.CacheElement;
import baseline.platform.ConsoleLog;
import baseline.platform.Log;
import baseline.request.Request;
import baseline.request.RequestException;

/**
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martinez</a>
 * */
public class CacheRequest {

	private CacheStorage storage;
	private PolicyReplacement policyReplacement;
	private Log log;

	public interface PolicyReplacement {
		boolean replace(long time, Object element);
	}

	public interface ResponseFilter {
		boolean keep(Request<?> request, Object element);
	}

	private ResponseFilter filter = new ResponseFilter() {

		@Override
		public boolean keep(Request<?> request, Object element) {
			return false;
		}

	};

	private static Log defaultLogger = new ConsoleLog();

	public CacheRequest() {
		this(new InMemoryCacheStorage());
	}

	public CacheRequest(CacheStorage storage) {
		this(storage, defaultLogger);
	}

	public CacheRequest(CacheStorage storage,
			PolicyReplacement policyReplacement) {
		this(storage, defaultLogger, policyReplacement);
	}

	public CacheRequest(CacheStorage storage, Log log) {
		this(storage, log, new PolicyReplacement() {

			@Override
			public boolean replace(long time, Object element) {
				return false; // default behavior, cache elements never are
								// replaced
			}
		});
	}

	public CacheRequest(CacheStorage storage, Log log,
			PolicyReplacement policyReplacement) {
		this.storage = storage;
		this.policyReplacement = policyReplacement;
		this.log = log;
	}

	public void setFilter(ResponseFilter filter) {
		if (filter == null) {
			return;
		}

		this.filter = filter;
	}

	@SuppressWarnings("unchecked")
	public <T> T exists(Request<T> request) {
		CacheElement ce = storage.get(request.getPath());
		return (ce != null) ? (T) ce.get() : null;
	}

	/**
	 * <p>
	 * Retrieve the object response for the current request from the cache. If
	 * the object is not found in the cache, a server call will be made to
	 * hopefully retrieve it.
	 * </p>
	 * <p>
	 * If the sever responds with null, the response won't be cached and a null
	 * will be returned to the caller.
	 * </p>
	 * 
	 * @param request
	 *            Request instance to execute
	 * @return response value
	 * @throws RequestException
	 */
	public <T> T get(Request<T> request) throws RequestException {

		String path = request.getPath();
		CacheElement ce = storage.get(path);

		if (ce != null) {
			if (!policyReplacement.replace(ce.getTime(), ce.get())) {
				@SuppressWarnings("unchecked")
				T t = (T) ce.get();
				log.info(String.format(
						"[cached] Path %s in cache with value %s", path,
						t.toString()));
				return t;
			}

			log.info(String
					.format("[expired] Path %s in cache but due policy replacement, we must update current element.",
							path));
		}

		// Need to request.
		T t = request.execute();

		if (t == null) {
			log.info(String
					.format("[invalid] null response we can't cache this."));
			return null; // We do not allow null values
		}

		if (filter.keep(request, t)) {
			// Adding response to Cache
			log.info(String.format(
					"[new] Path %s added to cache with value %s", path,
					t.toString()));
			storage.put(path, new CacheElement(t));
		}
		return t;
	}

	/**
	 * <p>
	 * Force the request against server, result will be cached if not null,
	 * previous key/value entry will be overwritten.
	 * </p>
	 * 
	 * @param request
	 *            instance to execute
	 * @return response value
	 * @throws RequestException
	 */
	public <T> T force(Request<T> request) throws RequestException {

		// Need to request.
		T t = request.execute();

		if (t == null) {
			return null; // We do not allow null values
		}

		if (filter.keep(request, t)) {
			String path = request.getPath();
			// Adding response to Cache
			log.info(String.format(
					"[force] Path %s entry overwritten in cache with value %s",
					path, t.toString()));
			storage.put(path, new CacheElement(t));
		}
		return t;
	}

	/**
	 * <p>
	 * Clears the App's Cache.
	 * </p>
	 */
	public void clear() {
		if (storage == null) {
			return;
		}

		storage.clear();
	}

}
